<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta name="viewport" content="width=960, initial-scale=0.6"/>
<meta name="keywords" content="Автомобили и запчасти HYUNDAI (Хундай), автомобили и запчасти KIA (Киа), автомобили и запчасти SSANGYONG (Ссангенг), корейские автомобили и запчасти, в Коломне, корейские автомобили, автомобили из кореи, продажа корейских авто, запчасти hyundai kia ssangyong хундай киа ссангенг, Корея-авто, описание, ремонт, обслуживание, заказ, опт, отзывы, Коломна, Московская область, запчасти для корейских автомобилей в Коломне" />
<meta name="description" content="Корейские автомобили. Запчасти hyundai, kia, ssangyong. Автомобили и запчасти HYUNDAI (Хундай), автомобили и запчасти KIA (Киа),  автомобили и запчасти SSANGYONG (Ссангенг), корейские автомобили и запчасти в Коломне." />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author"  content= "Snapix"  />

<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">
<link rel= "stylesheet"  href= "css/style.css"  type= "text/css " media= "screen" />
<link href='http://fonts.googleapis.com/css?family=Philosopher&subset=latin,cyrillic' rel='stylesheet' type='text/css'/>

<script type="text/javascript" src="http://api-maps.yandex.ru/2.0/?coordorder=longlat&load=package.full&wizard=constructor&lang=ru-RU"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=ru"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.js"></script>
<script type="text/javascript" src="js/2dtransform.js"></script>

<script type="text/javascript"> 
$(document).ready(function()
    {   $("body").animate({opacity: "1"}, 1000);

        $('.form').find('.cancel').hide(); 
        $('.item').find('.close').hide(); 
        
        //koreaAvto = new latlng(38.79830489658815, 55.07359283267593);
        $('.form').click(
        function openform()
        { $(this).animate({width: 600, height: 515, left: +300, top: +20}, 500).addClass('showed'); 
          $(this).find('.show').hide(500);
          $(this).find('.cancel').show(300);
        });
        
        $('.item').click(
        function openitem()
            {   $(document).find('.item_showed').removeClass('item_showed').addClass('item');
                $(this).addClass('item_showed').removeClass('item');
                $(this).find('.close').show(500);    
            }
        );
        
        $('.close').click(
        function closeitem()
        {   
            $(this).find('.close').hide(500);
            $(this).parent().removeClass('item_showed').addClass('item');
            $(this).hide(300);
            return false;
        }
        );
        
        $('.cancel').click(
        function()
        { $(this).parent().parent().find('.form').animate({width: 300, height: 55, left: 0, top: 0}, 500).removeClass('showed');
          $(this).parent().find('.show').show(300);
          $(this).hide(500);
          return false;
          }); 
  });
//global

var koreaAvto;
var userPos;          
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
            
function initialize()
    {
    	directionsDisplay = new google.maps.DirectionsRenderer();
        var map;
		var opts = {
     		 zoom: 12,
     		 center: new google.maps.LatLng(55.074817,38.764422),
     		 mapTypeId: google.maps.MapTypeId.ROADMAP,
             draggableCursor: 'default'
	         }

        map = new google.maps.Map(document.getElementById("map"), opts);
        directionsDisplay.setMap(map);
        
        koreaAvto = new google.maps.LatLng(55.073687,38.798497);
        var image = 'images/userPos.png';
        
        var marker = new google.maps.Marker({
        map: map,
        position: koreaAvto,
        icon: 'img/marker.png',
        title: 'Корея-Авто',
        zIndex: google.maps.Marker.MAX_ZINDEX + 1
      });
      //geolocation
   var userPosIcon = 'img/userPos.png';   

  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      userPos = new google.maps.LatLng(position.coords.latitude,
                                       position.coords.longitude);

      var marker = new google.maps.Marker({
        map: map,
        position: userPos,
        icon: userPosIcon,
        zIndex: google.maps.Marker.MAX_ZINDEX + 1
      });

    }, function() {
      handleNoGeolocation(true);
    });
  } else {
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);
  }  
}

    //calculating route to shop
    
    function calculateRoute(){
	    var start = userPos;
	    var end = koreaAvto;
	    
	     var request = {
      origin:start,
      destination:end,
      travelMode: google.maps.DirectionsTravelMode.DRIVING
  };
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
  
    }
    
    google.maps.event.addDomListener(window, 'load', initialize);
    // random image on background
    var backing = 4 + Math.round(Math.random());
    backimg = 'img/'+backing+'.jpg';
    console.log(backimg);
    $('#backimg').attr('src',backimg);
</script>

<title>Корея-Авто. Запчасти для корейских автомобилей. Запчасти hyundai, kia, ssangyong | Коломна и Московская область</title>
</head>


<body itemscope itemtype="http://schema.org/LocalBusiness">

<img id="backimg" style="position: absolute;  margin: 0 auto; width: 100%;" src="img/5.jpg" />
<div id="site-text">


    <include href="ui/header.html" />
    <include href="ui/nav.html" />

<span id="vert">
<div class="form" >
<span class="cancel">X</span>

<span class="show" style="top: 5px; position: relative">Нажмите здесь для доступа</span>
<span class="head">Форма быстрой связи</span>

<form id="fastmsg" name="fastmsg" action="message.php" method="post">
<table width="95%" border="0" style="position: relative; margin: 0 auto; text-align: left;">
<tr><td colspan="2"><hr /></td></tr>
<tr><td>Ваш вопрос касается</td>
<td><select name="quest" style="width: 100%;"> 
<option>Заказ запчастей</option>
<option>Работа магазина</option>
<option>Прочее</option>
</select></td></tr>
<tr><td><small>Если Ваш вопрос касается заказа автозапчастей и Вы знаете код интересующей Вас детали, введите этот код в поле справа</small></td>
<td valign="bottom"><input style="width: 100%;"  type="text" name="code"/></td></tr>
<tr><td colspan="2"><hr /></td></tr>
<tr><td colspan="2">Ваше сообщение:</td></tr>
<tr><td colspan="2"><textarea name="message" style="width: 100%; min-height: 200px; max-height: 230px; max-width: 600px;"></textarea> </td></tr>
<tr><td colspan="2"><hr /></td></tr>
<tr><td>Информация для связи с Вами:<br /><small>Вы можете указать контактный телефон, адрес электронной почты или любую другую удобную для Вас информацию</small></td>
<td valign="bottom"><small>Информация для связи</small><input  style="width: 100%;" type="text" name="from"/></td></tr>
<tr><td colspan="2"><hr /></td></tr>
<tr><td colspan="2" align="center">
<input id="sub" type="submit" value="Отправить"/>
</td></tr></table>
</form>
</div>

<div id="spec">


</div>

</span>

<div id="textbox">
    <p class='company_text'>
    <strong style="font-size: 26px;">
    Корея-Авто</strong> - один из <strong>крупнейших</strong> поставщиков
    комплектующих и запасных частей для корейских <br />
    автомобилей <strong>в Коломне и Московской области.</strong>
    <br /><br />
    На протяжении нескольких лет мы были и остаемся Вашими  надежными партнерами и просто профессионалами своего дела, 
    поставляя комплектующие и запасные части для Ваших автомобилей,консультируя по всевозможным вопросам и постоянно развиваясь,
    увеличивая ассортимент и повышая качество предоставления услуг.<br /><br />У нас Вы можете 
    <strong>купить автозапчасти</strong> и 
    комплектующие для корейских 
    автомобилей марки 
    <strong>HYUNDAI, KIA, SsangYong</strong><br /><br />
    Если интересующая Вас деталь отсутствует в нашем каталоге, мы осуществим ее доставку напрямую из Кореи!

    </p>
</div>

<div class="hrefs" style="float: left; position: relative; padding-left: 25px; display: block; text-align: right;">
    <a class="thref" onclick="$('.form').click()">Связаться с нами</a>
    <a class="thref" target="_blank" href="price.htm" title="Просмотреть прайс на сайте">Просмотреть каталог</a>
    <a class="thref" style="position: relative; top: 4px; left: -12px; padding-left: 8px!important; padding-top: 6px!important; padding-right: 9px !important;" href="price.xls" title="Скачать прайс в формате XLS"><img src="img/str.png" height="18" style="border: none; margin: 0; position: relative; padding: 0; " /></a>
    <a style="margin-left: 15px " class="thref" onclick="calculateRoute();">Как проехать?</a>
</div>


<div id="contacts" >

<div id="contact">
<span class="head">Наши контакты</span>
<span class="addres" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">

    <span itemprop="addressRegion">Московская область</span>, г.
    <span itemprop="addressLocality">Коломна</span>,
    <span itemprop="streetAddress">ул. Станкостроителей, ТЦ "Континент", 2А</span>

</span>
<span itemprop="telephone" class="phone">8 (926) 030 46 63</span>
<span itemprop="email" class="mail"> <img src="img/email.png" style="position: relative; margin-bottom: -15px; margin-right: 8px;"/>KoreaAvtozapchast@yandex.ru</span>
<span style="font-size: 1.3em;" ><img src="img/icq.png" style="position: relative; margin-bottom: -17px; margin-right: 8px;" />611 359 007</span><br /><br />
<span style="font-size: 1.3em; font-weight: bold; color: black; text-shadow: 0 0 4px white" ><img src="img/skype.png" style="position: relative; margin-bottom: -17px; margin-right: 8px;" />KoreaAvtozapchast</span>

</div>

<div id="ymaps-map-container" style="position: relative; box-shadow: 0 1px 5px black; margin: 30px -10px 0 5px; background: white; display: inline-block; padding: 6px;">
    <div id="map" style="width: 510px; height: 350px; "></div>
</div>
</div>
<hr style="clear: both; margin: 0; border: transparent solid 1px;" />
</div>

<?php include_once('footer.php'); ?>

</body>
</html>