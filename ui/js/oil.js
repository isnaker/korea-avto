/**
 * Created by Alex on 20.07.2016.
 */
function closeOverlay(){
    $('#overlay').remove();
}

function showOrderBox(){
    var overlay = document.createElement('div');
    overlay.id = 'overlay';
    //overlay.onclick = function () {closeOverlay()};

    var box = document.createElement('div');
    box.className = 'orderBox';
    box.innerHTML = '<h1>Сделайте заказ</h1><h2>и получите скидку 5%. </h2><p>Вы можете сделать заказ по телефону или электронной почте.</p>' +
        '<span>Наши контакты:</span> <h3>8 (926) 030 46 63</h3><h3>KoreaAvtozapchast@yandex.ru</h3>';
    box.style.marginTop = (parseInt($(document).width()) / 3) - 250 + 'px';

    var closeOverlayButton = document.createElement('button');
    closeOverlayButton.innerText = 'Закрыть';
    closeOverlayButton.className = 'closeOverlayButton';
    closeOverlayButton.onclick = function () {closeOverlay()};
    box.appendChild(closeOverlayButton);

    overlay.appendChild(box);
    $('body').append(overlay);
}

$(document).ready(function(){
    /* $('.oil-image').click(function(){
     var descr = $(this).parent().find('.oil-description');
     console.log(descr);
     $(descr).css('border','none');
     $(this).animate({'width':330},700);
     });*/

    $('.fancybox').fancybox();

    $('.thref').click(function(){
        showOrderBox();
    });

    //remove some table elems
    var oilparam = $('.oil-params');

    for (var i=0; i<(oilparam.length); i++){
        $(oilparam[i]).find('tr')[0].remove();
        $(oilparam[i]).find('tr')[0].remove();
    }
});