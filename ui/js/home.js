$(document).ready(function()
{   $("body").animate({opacity: "1"}, 1000);

    $('.form').find('.cancel').hide();
    $('.item').find('.close').hide();

    //koreaAvto = new latlng(38.79830489658815, 55.07359283267593);
    $('.form').click(
        function openform()
        { $(this).animate({width: 600, height: 515, left: +300, top: +20}, 500).addClass('showed');
            $(this).find('.show').hide(500);
            $(this).find('.cancel').show(300);
        });

    $('.item').click(
        function openitem()
        {   $(document).find('.item_showed').removeClass('item_showed').addClass('item');
            $(this).addClass('item_showed').removeClass('item');
            $(this).find('.close').show(500);
        }
    );

    $('.close').click(
        function closeitem()
        {
            $(this).find('.close').hide(500);
            $(this).parent().removeClass('item_showed').addClass('item');
            $(this).hide(300);
            return false;
        }
    );

    $('.cancel').click(
        function()
        { $(this).parent().parent().find('.form').animate({width: 300, height: 55, left: 0, top: 0}, 500).removeClass('showed');
            $(this).parent().find('.show').show(300);
            $(this).hide(500);
            return false;
        });
});
//global

var koreaAvto;
var userPos;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();

function initialize()
{
    directionsDisplay = new google.maps.DirectionsRenderer();
    var map;
    var opts = {
        zoom: 12,
        center: new google.maps.LatLng(55.074817,38.764422),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        draggableCursor: 'default'
    }

    map = new google.maps.Map(document.getElementById("map"), opts);
    directionsDisplay.setMap(map);

    koreaAvto = new google.maps.LatLng(55.073687,38.798497);
    var image = 'ui/images/userPos.png';

    var marker = new google.maps.Marker({
        map: map,
        position: koreaAvto,
        icon: 'ui/img/marker.png',
        title: 'Корея-Авто',
        zIndex: google.maps.Marker.MAX_ZINDEX + 1
    });
    //geolocation
    var userPosIcon = 'ui/img/userPos.png';

    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            userPos = new google.maps.LatLng(position.coords.latitude,
                position.coords.longitude);

            var marker = new google.maps.Marker({
                map: map,
                position: userPos,
                icon: userPosIcon,
                zIndex: google.maps.Marker.MAX_ZINDEX + 1
            });

        }, function() {
            handleNoGeolocation(true);
        });
    } else {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
    }
}

//calculating route to shop

function calculateRoute(){
    var start = userPos;
    var end = koreaAvto;

    var request = {
        origin:start,
        destination:end,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });

}

google.maps.event.addDomListener(window, 'load', initialize);
// random image on background
var backing = 4 + Math.round(Math.random());
backimg = 'ui/img/'+backing+'.jpg';
$('#backimg').attr('src',backimg);