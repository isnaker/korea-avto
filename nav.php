<?php
$link = $_SERVER['SCRIPT_NAME'];
$link = substr($link,1, -4);
$link = explode('/', $link);
$link = $link[count($link)-1];
// print $link;

function addcurrent()
{
    echo 'class="current"';
}
?>

<nav id="navigation">
    <a <?php if ($link == "index")         addcurrent(); ?>  href="/">Главная</a>
    <a <?php if ($link == "about-huindai") addcurrent(); ?> href="/about-huindai.php">О компании Huindai</a>
    <a <?php if ($link == "oil")           addcurrent(); ?> href="/oil.php">Автомасла</a>
    <a <?php if ($link == "accumulators")  addcurrent(); ?> href="/accumulators.php">Аккумуляторы</a>
    <a <?php if ($link == "contacts")      addcurrent(); ?> href="/contacts.php">Контакты</a>
</nav>