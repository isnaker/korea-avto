<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta name="viewport" content="width=960, initial-scale=0.6"/>
    <meta name="keywords" content="Автомобили и запчасти HYUNDAI (Хундай), автомобили и запчасти KIA (Киа), автомобили и запчасти SSANGYONG (Ссангенг), корейские автомобили и запчасти, в Коломне, корейские автомобили, автомобили из кореи, продажа корейских авто, запчасти hyundai kia ssangyong хундай киа ссангенг, Корея-авто, описание, ремонт, обслуживание, заказ, опт, отзывы, Коломна, Московская область, запчасти для корейских автомобилей в Коломне" />
    <meta name="description" content="Корейские автомобили. Запчасти hyundai, kia, ssangyong. Автомобили и запчасти HYUNDAI (Хундай), автомобили и запчасти KIA (Киа),  автомобили и запчасти SSANGYONG (Ссангенг), корейские автомобили и запчасти в Коломне." />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="author"  content= "Snapix"  />

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel= "stylesheet"  href= "ui/css/style.css"  type= "text/css " media= "screen" />
    <link href='http://fonts.googleapis.com/css?family=Philosopher&subset=latin,cyrillic' rel='stylesheet' type='text/css'/>

    <style type="text/css">
        #site-text {
            min-height: 800px;
        }

        #contacts {
            background: none;
        }
    </style>
    <script type="text/javascript" src="http://api-maps.yandex.ru/2.0/?coordorder=longlat&load=package.full&wizard=constructor&lang=ru-RU"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=ru"></script>
    <script type="text/javascript" src="/js/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="js/2dtransform.js"></script>
    <script type="text/javascript" src="/ui/js/contacts.js"></script>

    <title>Контакты. Корея-Авто. Запчасти для корейских автомобилей. Запчасти hyundai, kia, ssangyong | Коломна и Московская область</title>
</head>

<body itemscope itemtype="http://schema.org/LocalBusiness">

<img id="backimg" style="position: absolute;  margin: 0 auto; width: 100%;" src="img/5.jpg" />
<div id="site-text">

    <?php include_once('head.php'); ?>
    <?php include_once('nav.php'); ?>
    <div id="vert"></div>

    <div id="content">
        <h1>Контакты</h1>
    <div id="contacts">

        <div id="contact">
            <span class="head">Наши контакты</span>
            <span class="addres">Московская область, г. Коломна, ул. Станкостроителей, ТЦ "Континент", 2А</span>
            <span class="phone">8 (926) 030 46 63</span>
            <span class="mail"> <img src="img/email.png" style="position: relative; margin-bottom: -15px; margin-right: 8px;"/>KoreaAvtozapchast@yandex.ru</span>
            <span style="font-size: 1.3em;" ><img src="img/icq.png" style="position: relative; margin-bottom: -17px; margin-right: 8px;" />611 359 007</span><br /><br />
            <span style="font-size: 1.3em; font-weight: bold; color: black; text-shadow: 0 0 4px white" ><img src="img/skype.png" style="position: relative; margin-bottom: -17px; margin-right: 8px;" />KoreaAvtozapchast</span>
        </div>
        <div id="ymaps-map-container" style="position: relative; box-shadow: 0 1px 5px black; margin: 30px -10px 0 5px; background: white; display: inline-block; padding: 6px;">
            <div id="map" style="width: 510px; height: 350px; "></div>
        </div>
    </div>
    </div>
    <div style="clear: both"></div>
</div>
<?php include_once('footer.php'); ?>
</body>

</html>