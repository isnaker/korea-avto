<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta name="viewport" content="width=960, initial-scale=0.6"/>
    <meta name="keywords" content="Купить масло машинное, Купить масло трансмиссионное, масло хюндай, масло киа, масло kia, масло HYUNDAI,автомобильное масло купить, Автомобили и запчасти HYUNDAI (Хундай), автомобили и запчасти KIA (Киа), автомобили и запчасти SSANGYONG (Ссангенг), корейские автомобили и запчасти, в Коломне, корейские автомобили, автомобили из кореи, продажа корейских авто, запчасти hyundai kia ssangyong хундай киа ссангенг, Корея-авто, описание, ремонт, обслуживание, заказ, опт, отзывы, Коломна, Московская область, запчасти для корейских автомобилей в Коломне" />
    <meta name="description" content="Автомобили и запчасти HYUNDAI (Хундай), автомобили и запчасти KIA (Киа),  автомобили и запчасти SSANGYONG (Ссангенг), корейские автомобили и запчасти в Коломне." />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="author"  content= "Snapix"  />

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel= "stylesheet"  href= "/ui/css/style.css"  type= "text/css " media= "screen" />
    <link href='http://fonts.googleapis.com/css?family=Philosopher&subset=latin,cyrillic' rel='stylesheet' type='text/css'/>

    <link rel="stylesheet" href="js/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="/ui/css/oil.css">

    <script type="text/javascript" src="/js/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="/js/fancybox/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="/ui/js/oil.js"></script>
    <title>Автомасла. Корея-Авто. Запчасти для корейских автомобилей. Запчасти hyundai, kia, ssangyong | Коломна и Московская область</title>
    </head>

<body itemscope itemtype="http://schema.org/LocalBusiness">

<img id="backimg" style="position: absolute;  margin: 0 auto; width: 100%;" src="img/5.jpg" />
<div id="site-text">

<?php include_once('head.php'); ?>
<?php include_once('nav.php'); ?>
<div id="vert"></div>

    <div id="content">
    <h1>Автомасла</h1>
        <div id="oils">

        <article>
            <p>За нефтедобычу, нефтепереработку и изготовление смазочных материалов в чеболе отвечает компания Hyundai Oilbank.
            Именно Hyundai Oilbank поставляет (на конвейеры автозаводов и aftermarket) моторные и трансмиссионные масла,
            спецжидкости для АКПП, ГУР, тормозную жидкость, а также некоторые расходные материалы.</p>

            <p>Компания "<strong>Корея-Авто</strong>" предоставляет весь спектр смазочных материалов для автомобилей из Кореи. Имея возможность
            поставлять масла, запчасти и расходные материалы напрямую из Кореи, "<strong>Корея-Авто</strong>" дает Вам возможность выбирать
            необходимые Вам комплектующие из богатого, постоянно пополняемого ассортимента. Кроме того, Вы всегда можете сделать
            заказ на любую деталь, и мы осуществим ее доставку напрямую из Кореи!</p>
        </article>

        <h2 class="oil-category">Дизельные/Бензиновые моторные масла</h2>

            <div class="oil">
                <div class="oil-image">
                   <a href="img/oil/1.jpg" class="fancybox"> <img src="img/oil/1.jpg" alt="SANGYONG SAE-SW 30"> </a>
                </div>
                <div class="oil-description">
                    <h3 class="oil-caption">SSANGYONG SAE-SW 30</h3>

                    <table class="oil-params">
                        <tr><td>Класс:</td> <td> класс</td> </tr>
                        <tr><td>Вязкость:</td> <td> вязкость</td> </tr>
                        <tr><td>Фасовка:</td> <td> 4 литра</td> </tr>
                    </table>

                    <p class="oil-text">Ssang Yong  Масло моторное синтетическое "Diesel/Gasoline Fully 5W-30"
                        Высококачественное полностью синтетическое моторное масло для дизельных и бензиновых двигателей автомобилей SsangYong.
                    </p>
                </div>

                <div class="oil-controls">
                    <h2 class="oil-price">1500 р</h2>
                    <h4 class="oil-state">В наличии</h4>
                    <button class="thref">Заказать</button>
                    <small>Скидка 5% при заказе через сайт </small>
                </div>

            </div>


            <div class="oil">
                <div class="oil-image">
                    <a href="img/oil/2.jpg" class="fancybox">  <img src="img/oil/2.jpg" alt="SSANGYONG SAE-SW 30"> </a>
                </div>
                <div class="oil-description">
                    <h3 class="oil-caption">SSANGYONG SAE-SW 30</h3>

                    <table class="oil-params">
                        <tr><td>Класс:</td> <td> класс</td> </tr>
                        <tr><td>Вязкость:</td> <td> вязкость</td> </tr>
                        <tr><td>Фасовка:</td> <td> 1 литр</td> </tr>
                    </table>

                    <p class="oil-text">Масло для дизельных/бензиновых двигателей</p>
                </div>

                <div class="oil-controls">
                    <h2 class="oil-price">500 р</h2>
                    <h4 class="oil-state">В наличии</h4>
                    <button class="thref">Заказать</button>
                    <small>Скидка 5% при заказе через сайт </small>
                </div>

            </div>

            <div class="oil">
                <div class="oil-image">
                    <a href="img/oil/3.jpg" class="fancybox">  <img src="img/oil/3.jpg" alt="SSANGYONG MB 229.1"> </a>
                </div>
                <div class="oil-description">
                    <h3 class="oil-caption">SSANGYONG MB 229.1</h3>

                    <table class="oil-params">
                        <tr><td>Класс:</td> <td> класс</td> </tr>
                        <tr><td>Вязкость:</td> <td> вязкость</td> </tr>
                        <tr><td>Фасовка:</td> <td> 4 литра</td> </tr>
                    </table>

                    <p class="oil-text">Ssang Yong  Масло моторное полусинтетическое "All seasons Diesel/Gasoline 10W-40"
                        Высококачественное всесезонное моторное масло для дизельных и бензиновых двигателей автомобилей SsangYong.
                    </p>
                </div>

                <div class="oil-controls">
                    <h2 class="oil-price">1100 р</h2>
                    <h4 class="oil-state">В наличии</h4>
                    <button class="thref">Заказать</button>
                    <small>Скидка 5% при заказе через сайт </small>
                </div>

            </div>

        <h2 class="oil-category">Дизельные моторные масла</h2>

            <div class="oil">
                <div class="oil-image">
                    <a href="img/oil/4.jpg" class="fancybox">  <img src="img/oil/4.jpg" alt="HYUNDAI ACEA C3 SAE 5W/30"> </a>
                </div>
                <div class="oil-description">
                    <h3 class="oil-caption">HYUNDAI ACEA C3 SAE 5W/30</h3>

                    <table class="oil-params">
                        <tr><td>Класс:</td> <td> класс</td> </tr>
                        <tr><td>Вязкость:</td> <td> вязкость</td> </tr>
                        <tr><td>Фасовка:</td> <td> 6 литров</td> </tr>
                    </table>

                    <p class="oil-text">Hyundai/Kia Масло моторное синтетическое "Premium DPF Diesel 5W-30"
                        Новейшее синтетическое беззольное моторное масло разработано эксклюзивно для HYUNDAI и
                        KIA для новейших дизельных двигателей выпуска с 2007/08 года. Разработано для обеспечения
                        оптимального срока службы сажевого фильтра в дизельных двигателях. Это масло рекомендуются для
                        всех дизельных двигателей с сажевым фильтром. Соответствует высоким на сегодняшний день
                        требованиям HYUNDAI и KIA к качеству. Категория ACEA C3.
                        й</p>
                </div>

                <div class="oil-controls">
                    <h2 class="oil-price">1800 р</h2>
                    <h4 class="oil-state">В наличии</h4>
                    <button class="thref">Заказать</button>
                    <small>Скидка 5% при заказе через сайт </small>
                </div>

            </div>

            <div class="oil">
                <div class="oil-image">
                    <a href="img/oil/5.jpg" class="fancybox">  <img src="img/oil/5.jpg" alt="HYUNDAI ACEA C3 SAE 5W/30"> </a>
                </div>
                <div class="oil-description">
                    <h3 class="oil-caption">HYUNDAI ACEA C3 SAE 5W/30</h3>

                    <table class="oil-params">
                        <tr><td>Класс:</td> <td> класс</td> </tr>
                        <tr><td>Вязкость:</td> <td> вязкость</td> </tr>
                        <tr><td>Фасовка:</td> <td> 1 литр</td> </tr>
                    </table>

                    <p class="oil-text">Hyundai/Kia Масло моторное синтетическое "Premium DPF Diesel 5W-30"
                        Новейшее синтетическое беззольное моторное масло разработано эксклюзивно для HYUNDAI и
                        KIA для новейших дизельных двигателей выпуска с 2007/08 года. Разработано для обеспечения
                        оптимального срока службы сажевого фильтра в дизельных двигателях. Это масло рекомендуются для
                        всех дизельных двигателей с сажевым фильтром. Соответствует высоким на сегодняшний день
                        требованиям HYUNDAI и KIA к качеству. Категория ACEA C3.</p>
                </div>

                <div class="oil-controls">
                    <h2 class="oil-price">400 р</h2>
                    <h4 class="oil-state">В наличии</h4>
                    <button class="thref">Заказать</button>
                    <small>Скидка 5% при заказе через сайт </small>
                </div>

            </div>

        <h2 class="oil-category">Всесезонные моторные масла</h2>

            <div class="oil">
                <div class="oil-image">
                    <a href="img/oil/6.jpg" class="fancybox"> <img src="img/oil/6.jpg" alt="HYUNDAI ACEA A5 SAE 5W/30"></a>
                </div>
                <div class="oil-description">
                    <h3 class="oil-caption">HYUNDAI ACEA A5 SAE 5W/30</h3>

                    <table class="oil-params">
                        <tr><td>Класс:</td> <td> класс</td> </tr>
                        <tr><td>Вязкость:</td> <td> вязкость</td> </tr>
                        <tr><td>Фасовка:</td> <td> 1 литр / 4 литра</td> </tr>
                    </table>

                    <p class="oil-text">Hyundai/Kia Масло моторное синтетическое "ACEA A3 5W-30"
                        Современное синтетическое энергосберегающее моторное масло, рекомендованное к всесезонному
                        применению в двигателях автомобилей Hyundai и Kia с турбонаддувом и без него. Обязательно
                        к применению для обеспечения устойчивой работы двигателей с CVVT (система изменения фаз
                        газораспределения выпуска компании Hyundai), начиная с моделей двигателей 2L Beta I4
                        (автомобили Hyundai Elantra и Kia Spectra с 2005 года выпуска), Alpha II DOHC
                        (автомобили Hyundai Accent\Verna, Tiburon, Kia cee'd с 2006 года выпуска). Отличные
                        температурно- вязкостные характеристики обеспечивают надежную защиту двигателя от износа
                        во всем диапазоне рабочих температур. Синтетическая основа масла обеспечивает лёгкий
                        холодный пуск двигателя даже при экстремально низких температурах. Высокая стабильность
                        вязкостных характеристик в течение всего срока службы способствует экономии топлива.
                        Не оказывает вредного воздействия на самые современные системы очистки выхлопных газов.
                        Категории API SM, ILSAC GF-4, ACEA A3
                    </p>
                </div>

                <div class="oil-controls">
                    <h2 class="oil-price">1 л - 380 р</h2>
                    <h2 class="oil-price">4 л - 1400 р</h2>
                    <h4 class="oil-state">В наличии</h4>
                    <button class="thref">Заказать</button>
                    <small>Скидка 5% при заказе через сайт </small>
                </div>

            </div>

            <div class="oil">
                <div class="oil-image">
                    <a href="img/oil/7.jpg" class="fancybox"> <img src="img/oil/7.jpg" alt="HYUNDAI API SL/GF-III SAE 5W/30"></a>
                </div>
                <div class="oil-description">
                    <h3 class="oil-caption">HYUNDAI API SL/GF-III SAE 5W/30</h3>

                    <table class="oil-params">
                        <tr><td>Класс:</td> <td> класс</td> </tr>
                        <tr><td>Вязкость:</td> <td> вязкость</td> </tr>
                        <tr><td>Фасовка:</td> <td> 1 литр</td> </tr>
                    </table>

                    <p class="oil-text">Hyundai/Kia Масло моторное полусинтетическое "Super Extra Gasoline 5W-30"
                        Рекомендуется для всех типов бензиновых двигателей. Содержит тщательно подобранные присадки
                        для достижения оптимальных эксплуатационных характеристик. Его смазывающие свойства сохраняются
                        даже при интенсивном вождении благодаря качеству используемого базового масла и присадок.
                        Противодействует отложению сажи и нагара, способствует содержанию двигателя в чистоте и
                        обеспечивает отличное состояние сальников. Гарантирует лёгкий запуск и смазывание двигателя в
                        холодную погоду и представляет собой минеральное масло исключительного качества с превосходными
                        эксплуатационными характеристиками. Масло с добавлением высококачественных присадок,
                        поддерживает превосходные эксплутационные характеристики двигателя и малый расход топлива,
                        поэтому является самым распространенным маслом. Категория API SL, ILSAC GF-3.
                    </p>
                </div>

                <div class="oil-controls">
                    <h2 class="oil-price">320 р</h2>
                    <h4 class="oil-state">В наличии</h4>
                    <button class="thref">Заказать</button>
                    <small>Скидка 5% при заказе через сайт </small>
                </div>

            </div>

        <h2 class="oil-category">Трансмиссионное масло</h2>

            <div class="oil">
                <div class="oil-image">
                    <a href="img/oil/8.jpg" class="fancybox"> <img src="img/oil/8.jpg" alt="HYUNDAI API GL-4 SAE 80W/90"></a>
                </div>
                <div class="oil-description">
                    <h3 class="oil-caption">HYUNDAI API GL-4 SAE 80W/90 </h3>

                    <table class="oil-params">
                        <tr><td>Класс:</td> <td> класс</td> </tr>
                        <tr><td>Вязкость:</td> <td> вязкость</td> </tr>
                        <tr><td>Фасовка:</td> <td> 4 литра </td> </tr>
                    </table>

                    <p class="oil-text">Hyundai/Kia Масло трансмиссионное минеральное "Transmission Oil 80W-90"
                        Масло для механических трансмиссий SAE 80W-90 - универсальное трансмиссионное масло
                        разработаное на основе высококачественных минеральных масел специально для работы в
                        экстремальных условиях. Пакет самых современных присадок обеспечивает прекрасные
                        вязкостно-температурные характеристики масла, что гарантирует эффективную работу узлов
                        и деталей как механических трансмиссий, так и ведущих мостов автомобилей HYUNDAI и KIA.
                        Класс по API GL-4.
                    </p>
                </div>

                <div class="oil-controls">
                    <h2 class="oil-price">1300 р</h2>
                    <h4 class="oil-state">В наличии</h4>
                    <button class="thref">Заказать</button>
                    <small>Скидка 5% при заказе через сайт </small>
                </div>

            </div>

            <div class="oil">
                <div class="oil-image">
                    <a href="img/oil/9.jpg" class="fancybox">  <img src="img/oil/9.jpg" alt="HYUNDAI SP-III SAE 80W"> </a>
                </div>
                <div class="oil-description">
                    <h3 class="oil-caption">HYUNDAI SP-III SAE 80W</h3>

                    <table class="oil-params">
                        <tr><td>Класс:</td> <td> класс</td> </tr>
                        <tr><td>Вязкость:</td> <td> вязкость</td> </tr>
                        <tr><td>Фасовка:</td> <td> 4 литра</td> </tr>
                    </table>

                    <p class="oil-text">Hyundai/Kia Масло трансмиссионное полусинтетическое "ATF SP-III"
                        Полусинтетическое трансмиссионное масло для большинства четырёх и пятиступенчатых АКПП
                        автомобилей HYUNDAI и KIA. Обеспечивает плавное переключение передач в широком диапазоне
                        температур, обладает стабильностью фрикционных свойств, высокой текучестью при низких
                        температурах, хорошей совместимостью с большинством изделий из металлов и эластомеров,
                        обеспечивает защиту от протечек и износа.
                    </p>
                </div>

                <div class="oil-controls">
                    <h2 class="oil-price">1300 р</h2>
                    <h4 class="oil-state">В наличии</h4>
                    <button class="thref">Заказать</button>
                    <small>Скидка 5% при заказе через сайт </small>
                </div>

            </div>

            <div class="oil">
                <div class="oil-image">
                    <a href="img/oil/10.jpg" class="fancybox">  <img src="img/oil/10.jpg" alt="HYUNDAI SP-IV SAE 75W"> </a>
                </div>
                <div class="oil-description">
                    <h3 class="oil-caption">HYUNDAI SP-IV SAE 75W </h3>

                    <table class="oil-params">
                        <tr><td>Класс:</td> <td> класс</td> </tr>
                        <tr><td>Вязкость:</td> <td> вязкость</td> </tr>
                        <tr><td>Фасовка:</td> <td> 1 литр</td> </tr>
                    </table>

                    <p class="oil-text">Hyundai/Kia Масло трансмиссионное синтетическое "ATF SP-IV"
                        Cинтетическое трансмиссионное масло для 6-ступенчатых АКПП A6LF1/2/3, A6GF1, A6MF1/2.
                        Применяется в Hyundai: Elantra UD 2011- L4 1.6L - A6GF, Elantra UD 2011- L4 2.0L - A6MF1,
                        iX35 2004- L4 2.0L 2.4L - A6MF1, Santa Fe 2009 - L4 2.0L 2.2L V6 3.5L - A6LF1/2/3,
                        Santa Fe 2011- L4 2.4L V6 3.5L - A6LF2, Sonata 2009- L4 2.0L V6 3.5L - A6MF2,
                        Sonata 2010- L4 2.4L - A6MF2, Tucson 2009- L4 2.0L - A6LF1/2, Tucson 2009- L4 2.0L 2.4L - A6MF1,
                        Veracruz 2011- V6 3.0 - A6LF3, Grandeur HG/Azera TG 2011- V6 3.3L 3.8L A6LF1/2;
                        KIA Sorento 2009- V6 3.3L 3.5L 3.8L - A6LF2/A6MF2, Sportage 2010- L4 2.0L - A6LF2,
                        Sportage 2010- L4 2.4L - A6MF1, Forte/Forte Koup 2010- L4 2.0L 2.4L - A6MF1,
                        Optima TF HEV 2011- L4 2.4L – A6MF2H, Optima TF 2011- L4 2.0L – A6LF2,
                        Optima TF 2011- L4 2.4L – A6MF2, Sedona VQ 2011- V6 3.5L – A6LF2,
                        Sorento XM 2011 – L4 2.4L – A6MF2, Sorento XM 2011 – V6 3.5L – A6LF2
                    </p>
                </div>

                <div class="oil-controls">
                    <h2 class="oil-price">400 р</h2>
                    <h4 class="oil-state">В наличии</h4>
                    <button class="thref">Заказать</button>
                    <small>Скидка 5% при заказе через сайт </small>
                </div>

            </div>

            <div class="oil">
                <div class="oil-image">
                    <a href="img/oil/11.jpg" class="fancybox">  <img src="img/oil/11.jpg" alt="HYUNDAI PSF-4 SAE 80W"> </a>
                </div>
                <div class="oil-description">
                    <h3 class="oil-caption">HYUNDAI PSF-4 SAE 80W</h3>

                    <table class="oil-params">
                        <tr><td>Класс:</td> <td> класс</td> </tr>
                        <tr><td>Вязкость:</td> <td> вязкость</td> </tr>
                        <tr><td>Фасовка:</td> <td> 1 литр</td> </tr>
                    </table>

                    <p class="oil-text">Hyundai/Kia Жидкость гур синтетическое "Ultra PSF-4"
                        Специальная синтетическая жидкость зелёного цвета для гидроусилителя руля автомобилей HYUNDAI и KIA.
                    </p>
                </div>

                <div class="oil-controls">
                    <h2 class="oil-price">500 р</h2>
                    <h4 class="oil-state">В наличии</h4>
                    <button class="thref">Заказать</button>
                    <small>Скидка 5% при заказе через сайт </small>
                </div>

            </div>

            <div class="oil">
                <div class="oil-image">
                    <a href="img/oil/12.jpg" class="fancybox"> <img src="img/oil/12.jpg" alt="HYUNDAI API GL-4 SAE 75W/90"> </a>
                </div>
                <div class="oil-description">
                    <h3 class="oil-caption">HYUNDAI API GL-4 SAE 75W/90</h3>

                    <table class="oil-params">
                        <tr><td>Класс:</td> <td> класс</td> </tr>
                        <tr><td>Вязкость:</td> <td> вязкость</td> </tr>
                        <tr><td>Фасовка:</td> <td> 1 литр</td> </tr>
                    </table>

                    <p class="oil-text">Hyundai/Kia Масло трансмиссионное синтетическое "Gear Oil 75W-90"
                        Синтетическое трансмиссионное масло Gear Oil SAE75W-90 создано на основе специально
                        составленной композиции базовых масел и тщательно сбалансированного пакета высокоэффективных
                        присадок. Оно подходит для использования в механических коробках передач и редуктарах рулевых
                        механизмов. Класс по API GL-3/4.
                    </p>
                </div>

                <div class="oil-controls">
                    <h2 class="oil-price">350 р</h2>
                    <h4 class="oil-state">В наличии</h4>
                    <button class="thref">Заказать</button>
                    <small>Скидка 5% при заказе через сайт </small>
                </div>

            </div>

            <div class="oil">
                <div class="oil-image">
                    <a href="img/oil/13.jpg" class="fancybox"> <img src="img/oil/13.jpg" alt="HYUNDAI MTF PRIME 75W-85"> </a>
                </div>
                <div class="oil-description">
                    <h3 class="oil-caption">HYUNDAI MTF PRIME 75W-85</h3>

                    <table class="oil-params">
                        <tr><td>Класс:</td> <td> класс</td> </tr>
                        <tr><td>Вязкость:</td> <td> вязкость</td> </tr>
                        <tr><td>Фасовка:</td> <td> 1 литр</td> </tr>
                    </table>

                    <p class="oil-text">Hyundai/Kia Масло трансмиссионное полусинтетическое "MTF PRIME 75W-85"
                        Всесезонное полусинтетическое трансмиссионное масло, совместимо со всеми материалами
                        уплотнений и эластомерными материалами, а также с металлами и сплавами,  используемыми
                        в трансмиссиях автомобилей Hyudai и Kia. Тщательно подобранный высокоэффективный пакет
                        присадок и синтетическая технология производства базовых масел позволяют сохранять
                        исключительную текучесть при низких температурах и прекрасные синхронизирующие
                        свойства в различных эксплуатационных условиях, одновременно обеспечивая лёгкое
                        переключение передач. Продукт обладает великолепными вязкостными и противоизносными
                        характеристиками, благодаря новейшей технологии присадок обеспечивает сверхвысокую
                        защиту от износа в широком диапазоне температур. Обеспечивает очень легкое
                        переключение передач, обладает высокими синхронизирующими свойствами одновременно
                        с надежной защитой от износа гипоидных и цилиндрических передач. Обладает превосходными
                        низкотемпературными свойствами, гарантирует смазывание всех пар трения даже при
                        очень низких температурах. Высокие антиокислительные свойства гарантируют чистоту
                        деталей трансмиссии.

                    </p>
                </div>

                <div class="oil-controls">
                    <h2 class="oil-price">350 р</h2>
                    <h4 class="oil-state">В наличии</h4>
                    <button class="thref">Заказать</button>
                    <small>Скидка 5% при заказе через сайт </small>
                </div>

            </div>





            <div style="clear: both; width: 100%; height: 60px"></div>
        </div>
    </div>

<div style="clear: both"></div>
</div>
<?php include_once('footer.php'); ?>
</body>

</html>