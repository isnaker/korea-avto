<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta name="viewport" content="width=960, initial-scale=0.6"/>
    <meta name="keywords" content="Автомобили и запчасти HYUNDAI (Хундай), автомобили и запчасти KIA (Киа), автомобили и запчасти SSANGYONG (Ссангенг), корейские автомобили и запчасти, в Коломне, корейские автомобили, автомобили из кореи, продажа корейских авто, запчасти hyundai kia ssangyong хундай киа ссангенг, Корея-авто, описание, ремонт, обслуживание, заказ, опт, отзывы, Коломна, Московская область, запчасти для корейских автомобилей в Коломне" />
    <meta name="description" content="Корейские автомобили. Запчасти hyundai, kia, ssangyong. Автомобили и запчасти HYUNDAI (Хундай), автомобили и запчасти KIA (Киа),  автомобили и запчасти SSANGYONG (Ссангенг), корейские автомобили и запчасти в Коломне." />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="author"  content= "Snapix"  />

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel= "stylesheet"  href= "ui/css/style.css"  type= "text/css " media= "screen" />
    <link href='http://fonts.googleapis.com/css?family=Philosopher&subset=latin,cyrillic' rel='stylesheet' type='text/css'/>

    <style type="text/css">
        #site-text {
            min-height: 800px;
        }

        #about-huindai {
            margin-bottom: 80px;
            width: 95%;
            line-height: 1.35;
        }

        #portrait {
            float: right;
            width: 300px;
            margin: 1px 0 15px 15px;
            box-shadow: 0 3px 5px gray;
            border: white solid 4px;
        }

        #portrait img {
            width: 100%;
        }
    </style>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.js"></script>
    <title>О компании Huindai. Корея-Авто. Запчасти для корейских автомобилей. Запчасти hyundai, kia, ssangyong | Коломна и Московская область</title>
</head>

<body itemscope itemtype="http://schema.org/LocalBusiness">

<img id="backimg" style="position: absolute;  margin: 0 auto; width: 100%;" src="img/5.jpg" />
<div id="site-text">

    <?php include_once('head.php'); ?>
    <?php include_once('nav.php'); ?>
    <div id="vert"></div>

    <div id="content">
        <h1>О компании Huindai</h1>

        <iframe width="800" height="310" src="//www.youtube.com/embed/lNva2MIeeLM" frameborder="0" allowfullscreen></iframe>

        <div id="about-huindai">
            <p>
                Компания MOBIS, основанная в июле 1977 года,  входит в группу компаний Hyundai&Kia и является крупнейшей
                компанией в Южной Корее по разработке, производству и распространению автокомпонентов для автомобилей Hyundai и Kia.
            </p>


            <p> <strong>Hyundai</strong> — это группа компаний, основанная Chung Ju-yung в Южной Корее. Само слово "Hyundai" с корейского
            можно перевсти как "современность" или "новое время". Первая компания Hyundai Construction была создана в
            1947 году и занималась строительством. В результате бурного развития группа компаний Hyundai превратилась в
            крупнейший чеболь (форма бизнес-конгломератов, представляющая собой группу формально самостоятельных фирм)
            Южной Кореи. В сферу интересов чеболя входят строительство, контейнерные перевозки, автомобилестроение,
            туризм, нефтедобыча, кораблестроение, гостиничный бизнес, тяжелое машиностроение, финансы, электроника и
            многое другое.</p>

            <div id="portrait">
                <img src="img/portrait.jpg" alt="Chung Ju-yung">
            </div>

            <p>Наиболее известным представителем чеболя в нашей стране является Hyundai Motor Company. Hyundai Kia
            Automotive Group, в состав которой, как легко понять из названия :), входят Hyundai Motor Company и Kia
            Motors, является вторым по размерам автопроизводителем в Азии (после Toyota Motor) и четвертым в мире
            (также после GM и Volkswagen).</p>

            <p>Hyundai Motor Company была основана Hyundai Construction в декабре 1967 года. Именно с этой даты и
            начинается история развития (и успехов) южнокорейского автопрома. Молодая компания начинала с изготовления
            по лицензии британского отделения Ford автомобилей и малотоннажных грузовиков. Изначально собирались седаны
            Ford Cortina, Ford Granada и восьмитонные грузовики Ford D-750. Об автомобильной марке Hyundai речи тогда не
            шло: капоты машин украшала надпись Ford.</p>

            <p>Однако уже в начале 70-х годов было принято решение о разработке собственного легкового автомобиля и в 1973
            году в компании открылся собственный конструкторский отдел. В 1975 году совместно с инженерами японского
            Mitsubishi Motors (и на основе Mitsubishi Colt) была разработана первая собственная модель Hyundai Pony.
            Подкрепленный дизайном самого Giorgio Giugiaro и его ItalDesign автомобиль покорил рынок Южной Кореи.
            Воодушевлённые корейцы тут же начали экспорт Pony под именем Excel — сначала в Эквадор, а затем и в другие
            страны.</p>

            <p>Доминирование на внутреннем рынке стало безоговорочным после покупки в 1998 году компании Kia/Asia Motors.
            Завод, которым располагает Hyundai Motor в Ульсане, Южная Корея, на сегодня является крупнейшим в мире:
            он способен ежегодно выпускать до 1.5 млн автомобилей в год. Кроме него в Корее компания располагает
            заводами Чонджу и Асане. Самый крупный зарубежный завод построен в 1998 году в Ченнай, Индия, —
            он выпускает до 120 тыс автомобилей в год. Кроме него компания располагает сборочными производствами в
            Китае, Египте, Венесуэле, Малайзии, Пакистане, Индонезии и Тайване.</p>

            <p>В России Hyundai Motor Company начала с создания в августе 2001 года совместного с ФПГ «Донинвест»
            предприятия по выпуску Hyundai Accent. В настоящее время (март 2010 года) уже начато строительство
            собственного предприятия в Санкт-Петербурге. В предприятие планируется инвестировать порядка 500 миллионов
            евро, запуск коммерческого производства завода планируется на начало 2011 года.</p>
        </div>

    </div>

    <div style="clear: both"></div>
</div>
<?php include_once('footer.php'); ?>
</body>

</html>