<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta name="viewport" content="width=960, initial-scale=0.6"/>
    <meta name="keywords" content="Автомобили и запчасти HYUNDAI (Хундай), автомобили и запчасти KIA (Киа), автомобили и запчасти SSANGYONG (Ссангенг), корейские автомобили и запчасти, в Коломне, корейские автомобили, автомобили из кореи, продажа корейских авто, запчасти hyundai kia ssangyong хундай киа ссангенг, Корея-авто, описание, ремонт, обслуживание, заказ, опт, отзывы, Коломна, Московская область, запчасти для корейских автомобилей в Коломне" />
    <meta name="description" content="Корейские автомобили. Запчасти hyundai, kia, ssangyong. Автомобили и запчасти HYUNDAI (Хундай), автомобили и запчасти KIA (Киа),  автомобили и запчасти SSANGYONG (Ссангенг), корейские автомобили и запчасти в Коломне." />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="author"  content= "Snapix"  />

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <?php echo $this->render('/ui/common_css.html',$this->mime,get_defined_vars()); ?>
    <link href='http://fonts.googleapis.com/css?family=Philosopher&subset=latin,cyrillic' rel='stylesheet' type='text/css'/>

    <script type="text/javascript" src="http://api-maps.yandex.ru/2.0/?coordorder=longlat&load=package.full&wizard=constructor&lang=ru-RU"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=ru"></script>
    <script type="text/javascript" src="/ui/js/jquery.js"></script>
    <script type="text/javascript" src="/ui/js/2dtransform.js"></script>
    <script type="text/javascript">
        $(function(){
            $('#a_catalog').addClass('current');
        });
    </script>
    <title>Корея-Авто. Запчасти для корейских автомобилей. Запчасти hyundai, kia, ssangyong | Коломна и Московская область</title>
</head>


<body itemscope itemtype="http://schema.org/LocalBusiness">

<img id="backimg" style="position: absolute;  margin: 0 auto; width: 100%;" src="/ui/img/5.jpg" />

<div id="site-text">
    <?php echo $this->render('/ui/header.html',$this->mime,get_defined_vars()); ?>
    <?php echo $this->render('/ui/nav.html',$this->mime,get_defined_vars()); ?>
        <div id="vert"></div>
        <div id="content">
            <div id="breadcrumbs">
                <h1><a class="rounded_button" href="/catalog">Каталог</a></h1>
                    <?php if (isset($PARAMS['categoryId'])): ?>
                        <?php if (($current['parent_id'] != 0)): ?>> <a class="rounded_button" href="/catalog/<?php echo $current['parent_id']; ?>"><?php echo $parent['name']; ?></a> <?php endif; ?>
                        > <strong><?php echo $current['name']; ?></strong>
                    <?php endif; ?>
            </div>

    <div class="row">
        <?php if (isset($categories) && count($categories) > 0): ?>
            <h2>Подкатегории: </h2>
            <ul id="categories" class="items">
                <?php foreach (($categories?:array()) as $category): ?>
                    <li>
                        <a href="/catalog/<?php echo $category['id']; ?>">
                        <?php if (isset($category['image'])): ?>
                            <img src="/<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>">
                            <?php else: ?><img src="/images/no_image.jpg" alt="">
                        <?php endif; ?>
                        <p><?php echo $category['name']; ?></p>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
            <div class="clearfix"></div>
        <?php endif; ?>
    </div>

    <?php if (isset($products) && count($products) > 0): ?>
        
            <h3 style="margin: 1.2em 0">Продукты в категории "<?php echo ($current['name']); ?>"</h3>
            <ul class="items">
                <?php foreach (($products?:array()) as $product): ?>
                    <li>
                        <a href="/catalog/products/<?php echo $product['id']; ?>">
                        <?php if (isset($product['image']) && ($product['image'] != '')): ?>
                            <img src="/<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>">
                            <?php else: ?><img src="/images/no_image.jpg" alt="">
                        <?php endif; ?>
                        <p><?php echo $product['name']; ?></p></a></li>
                <?php endforeach; ?>
            </ul>
            <div class="clearfix"></div>
        
        <?php else: ?>
            <?php if (isset($current)): ?><div style="padding-top: 40px; font-size: 1.2em" class="text-centered"><p>В данной категории нет товаров</p> <a class="rounded_button" href="/catalog/<?php echo $current['parent_id']; ?>"> < Вернуться назад</a> </div><?php endif; ?>
        
    <?php endif; ?>

    <div class="row">
    <?php if (isset($PARAMS['categoryId'])): ?>
        <?php if ($current['parent_id'] == 0): ?>
            
                <div class="maincategory">
                    <h1><?php echo $current['name']; ?></h1>
                    <div class="row"><p><?php if (isset($current_image) && ($current_image != '')): ?>
                        <img style="max-width: 400px; float: left; margin: 0 3% 3% 0" src="/<?php echo $current_image['url']; ?>" alt="<?php echo $current['name']; ?>"><?php endif; ?>
                    <?php echo $current['description']; ?></p></div>
                </div>
            
            <?php else: ?>

                <div class="row"><p><?php echo $current['description']; ?></p></div>
            

        <?php endif; ?>


    <?php endif; ?>
        <div class="clearfix"></div>
    </div>
</div>

<?php echo $this->render('ui/footer.html',$this->mime,get_defined_vars()); ?>

</body>
</html>