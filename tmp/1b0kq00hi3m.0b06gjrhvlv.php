<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>

    <meta name="viewport" content="width=1120"/>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="author" content="Snaker" />
    <meta name="keywords" content="Кит-авто, запчасти для китайских автомобилей коломна, запчасти для автомобилей китайского производства, запчасти Chery,запчасти Geely,запчасти Great Wall, запчасти Lifan,автозапчасти для тюнинга китайских автомобилей,запчасти ЧЕРИ, запчасти ГРЕЙТ ВОЛ,запчасти лифан,запчасти Коломна, Воскресенск" />
    <meta name="description" content="Компания Кит-Авто. Запчасти для китайских автомобилей. Обслуживание, тюнинг и ремонт. Московская область, Коломна." />

    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href="/ui/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="/ui/css/admin.css"/>
    <title>Корея-Авто</title>
    <link rel="stylesheet" type="text/css" href="/ui/css/framework.css"/>
    <script type="text/javascript" src="/ui/js/jquery.js"></script>
    <script type="text/javascript" src="/ui/js/jquery.easing.1.3.js"></script>
    <script>
        $(function(){
           $('.categoryItem').hover(function(){
               $(this).find('.categoryAdmin').addClass('showed');
           }, function(){
               $(this).find('.categoryAdmin').removeClass('showed');
           });

           $('.categoryMenu li>.categoryBlock').click(function(){
               var thisUl = $(this).parent('li').children('ul');
               var thisDisplay = (thisUl).css('display');
               if(thisDisplay=='none'){
                   $(thisUl).css('display','block');
               }else{
                   $(thisUl).css('display','none');
               }
           });
        });
    </script>
</head>
<body>

<div id="doc" class="centered row">
    <a href="/">< Вернуться на сайт</a>
    <h4 style="float: right">Приветствую,  <?php echo $username; ?>! [<a href="/logout">Выйти</a> ]</h4>
    <h1>Управление сайтом</h1>
    <hr>

    <div class="row centered">
        <div class="box33p">
            <h2>Управление категориями товаров</h2>
            <a class="rounded_button" href="/admin/addCategory"> <i class="fa fa-plus-square-o"></i> Добавить новую категорию</a>
            <div class="clearfix"></div>
            
            <div class="categoryMenu" id="categories">
                <?php echo $categoriesGrup; ?>
            </div>
            
        </div>

        <div class="box3p"></div>
        <div class="box60p">
            <div class="box50p">
                <h2>Управление товарами</h2>
                <a class="rounded_button" href="/admin/newProduct"> <i class="fa fa-plus-square-o"></i> Добавить новый товар</a>
            </div>
            <div class="box50p">
                <h3>Показывать только из категории</h3>
                <form method="get" action="/admin">
                    <select id="productsByCategoryLister" name="groupId">
                        <?php foreach (($categories?:array()) as $category): ?>
                            <option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
                        <?php endforeach; ?>
                    </select>
                <input type="submit" class="button" value="go">
                </form>
            </div>
            <div class="clearfix"></div>

            <ul class="clearfix">
                <?php foreach (($products?:array()) as $product): ?>
                    <li class="categoryItem clearfix">
                        <div class="categoryBlock">
                            <div class="box1p">
                                <?php if (isset($product['image'])): ?><img height="40" src="/<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>"><?php endif; ?>
                            </div>
                            <div class="box1p"></div>
                            <div class="box75p categoryName"><?php echo $product['name']; ?></div>
    
                            <div class="categoryAdmin">
                                <a class="edit" href="/admin/editProduct/<?php echo $product['id']; ?>"> <i class="fa fa-pencil-square-o"></i> редактир.</a>
                                <a class="del" href="/admin/removeProduct/<?php echo $product['id']; ?>"> <i class="fa fa-close"></i> удалить</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="clearfix"></div>

</div>


<div class="clearfix"></div>
</div>
</body>
</html>