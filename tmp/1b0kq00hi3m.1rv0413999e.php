<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>

    <meta name="viewport" content="width=1120"/>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="author" content="Snaker" />
    <meta name="keywords" content="Кит-авто, запчасти для китайских автомобилей коломна, запчасти для автомобилей китайского производства, запчасти Chery,запчасти Geely,запчасти Great Wall, запчасти Lifan,автозапчасти для тюнинга китайских автомобилей,запчасти ЧЕРИ, запчасти ГРЕЙТ ВОЛ,запчасти лифан,запчасти Коломна, Воскресенск" />
    <meta name="description" content="Компания Кит-Авто. Запчасти для китайских автомобилей. Обслуживание, тюнинг и ремонт. Московская область, Коломна." />

    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href="/ui/css/framework.css"/>

    <?php echo $this->render('/ui/common_css.html',$this->mime,get_defined_vars()); ?>
    <link rel="stylesheet" type="text/css" href="/ui/css/admin.css"/>
    <title>Кит-Авто - Запчасти для китайских автомобилей в Коломне</title>

    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
    <style>
        #doc {width: auto}
        #content {width: auto}
        textarea {min-width: 450px}
    </style>
</head>
<body>

<div id="doc" class="row centered">

    <div class="box1100" id="content">
    <a class="rounded_button" href="/admin">< Назад</a>

    <?php if (($product['id'] <> "")): ?>
        <h1>Редактирование товара</h1>
        <?php else: ?><h1>Добавление нового товара</h1>
    <?php endif; ?><hr>

    <form action="/admin/writeProduct" enctype="multipart/form-data" method="post">
        <?php if (isset($product['id']) && ($product['id'] != '')): ?>
            <input hidden="hidden" name="id" value="<?php echo $product['id']; ?>">
        <?php endif; ?>

    <div class="box60p">

    <label for="name">Заголовок</label><br>
    <input name="name" id="name" value="<?php echo $product['name']; ?>"><br>

    <label for="article">Артикул</label><br>
    <input name="article" id="article" value="<?php echo $product['article']; ?>"><br>

    <label for="price">Цена</label><br>
    <input name="price" id="price" value="<?php echo $product['price']; ?>"><br>

        <label for="category">Категория</label><br>
        <select id="category" name="category">
        <?php foreach (($categories?:array()) as $category): ?>
            <option value="<?php echo $category['id']; ?>" <?php if ($category['id'] == $product['category']): ?>selected="selected"<?php endif; ?> ><?php echo $category['name']; ?></option>
        <?php endforeach; ?>
        </select>

    <br><br><label for="hidden">Видимость</label><br>
        <select id="hidden" name="hidden">
            <option value="0" <?php if ($product['hidden'] === 0): ?>selected="selected"<?php endif; ?> >Виден пользователям</option>
            <option value="1" <?php if ($product['hidden'] === 1): ?>selected="selected"<?php endif; ?> >Скрыт от просмотра</option>
        </select>

        <br>
    <div id="Text">
        <textarea name='description' id='description' rows='11' cols='20'><?php echo $product['description']; ?></textarea>
    </div>
    </div>

    <div class="box33p">
        <p>Изображение товара</p>
        <?php if (isset($current_product_image) && ($current_product_image['url'] != '')): ?>
            <img width="300" src="/<?php echo $current_product_image['url']; ?>" alt="">
            <?php else: ?><img width="300" src="/images/no_image.jpg" alt="">
        <?php endif; ?>
        <hr>выберите новое изображение, если необходимо
        <input type="file" name="image">

        <div class="row" style="margin-top: 1.2em">
            <h4>Наличие на складе</h4>

            <select name="state">
            <?php foreach (($product_states?:array()) as $product_state): ?>
                <option value="<?php echo $product_state['id']; ?>" <?php if ($product_state['id'] == $product['state']): ?>selected="selected"<?php endif; ?> ><?php echo $product_state['name']; ?></option>
            <?php endforeach; ?>
            </select>
        </div>

    </div>
    <div class="clearfix"></div>
    <input type='submit' value='Сохранить изменения'/>
    </form>
        <div class="clearfix"></div>
</div>
</body>

</html>