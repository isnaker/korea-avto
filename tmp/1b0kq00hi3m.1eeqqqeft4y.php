<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>

    <meta name="viewport" content="width=1120"/>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="author" content="Snaker" />
    <meta name="keywords" content="Кит-авто, запчасти для китайских автомобилей коломна, запчасти для автомобилей китайского производства, запчасти Chery,запчасти Geely,запчасти Great Wall, запчасти Lifan,автозапчасти для тюнинга китайских автомобилей,запчасти ЧЕРИ, запчасти ГРЕЙТ ВОЛ,запчасти лифан,запчасти Коломна, Воскресенск" />
    <meta name="description" content="Компания Кит-Авто. Запчасти для китайских автомобилей. Обслуживание, тюнинг и ремонт. Московская область, Коломна." />

    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href="/ui/css/framework.css"/>

    <?php echo $this->render('/ui/common_css.html',$this->mime,get_defined_vars()); ?>
    <link rel="stylesheet" type="text/css" href="/ui/css/admin.css"/>
    <title>Кит-Авто - Запчасти для китайских автомобилей в Коломне</title>

    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
    <style>
        #doc {width: auto}
        .hidden {display: none}
        textarea {min-width: 450px; min-height: 200px;}
        input, select {margin-bottom: 10px}
        #shownInSidebar {margin-top: 15px}
    </style>
    <script>
        $(function(){
            $('#shownInSidebar_controller').click(function(){
                $('#shownInSidebar').toggleClass('hidden');
            });
        })
    </script>
</head>
<body>

<div id="doc" class="row centered">

<div class="box1100 centered" id="content">
    <a class="rounded_button" href="/admin">< Назад</a>
    <?php if (isset($current_category)): ?>
        <h1>Изменение раздела</h1>
        <?php else: ?><h1>Добавление нового раздела</h1>
    <?php endif; ?>
    <div style="height: 30px"></div>
    <form action="/admin/saveCategory" method="post" enctype="multipart/form-data">

        <div class="box50p">
        <?php if (isset($current_category)): ?>
            <input hidden="hidden" name="id" value="<?php echo $current_category['id']; ?>">
        <?php endif; ?>

        <label for="">Заголовок</label><br>
        <input type="text" name="name" id=""
            <?php if (isset($current_category)): ?>
                value="<?php echo $current_category['name']; ?>"
                <?php else: ?>value=""
            <?php endif; ?>>

        <br><label for="postCategory">Родительская категория</label><br>
        <select id="postCategory" name="parent_id">
            <option value="0"><Без категории></option>
            <?php foreach (($categories?:array()) as $category): ?>
                <option value="<?php echo $category['id']; ?>"
                <?php if (isset($current_category)): ?>
                    <?php if ($current_category['parent_id'] == $category['id']): ?>selected="selected"<?php endif; ?>
                <?php endif; ?> ><?php echo $category['name']; ?>
                </option>
            <?php endforeach; ?>
        </select>

        <br><label for="postInvisible">Видимость категории</label><br>
        <select id="postInvisible" name="hidden">
            <option value="1"
            <?php if (isset($current_category)): ?><?php if ($current_category['hidden'] === 1): ?>selected="selected"<?php endif; ?><?php endif; ?> >Скрыта от просмотра</option>
            <option value="0"
            <?php if (isset($current_category)): ?><?php if ($current_category['hidden'] === 0): ?>selected="selected"<?php endif; ?><?php endif; ?> >Видна пользователям</option>
        </select>
        <br>
            <label for="description">Описание категории</label><br>
            <textarea id="description" name="description"><?php if (isset($current_category)): ?><?php echo $current_category['description']; ?><?php endif; ?></textarea>
        <br><br>
            <input type="checkbox" id="shownInSidebar_controller"> Отображать категорию в меню справа? (Только для категорий верхнего уровня)
            <div id="shownInSidebar" class="hidden">
                <div>Выберите иконку для бокового меню (30х30 пикселей) </div>
                <input type="file" name="logo_ur">
            </div>
            <hr>

        <?php if (isset($current_category)): ?>
            <input type="submit" value="Обновить раздел">
            <?php else: ?><input type="submit" value="Создать раздел">
        <?php endif; ?>

        </div>

        <div class="box40p">
            <h3>Изображение категории</h3>
            <?php if (isset($current_category_image) && ($current_category_image['url'] != '')): ?>
                <img width="300" src="/<?php echo $current_category_image['url']; ?>" alt="">
                <?php else: ?><img width="300" src="/images/no_image.jpg" alt="">
            <?php endif; ?>
            <div>Выберите новое изображение</div>
            <input type="file" name="image">
        </div>
        <div class="clearfix"></div>
    </form>
    <div class="clearfix"></div>
</div>
    <div class="clearfix"></div>
    </div>
</body>
</html>