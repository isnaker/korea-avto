<?php
/**
 * Created by PhpStorm.
 * User: iSnaker
 * Date: 08.10.14
 * Time: 12:17
 */


class Backend {
    private $db;
    public function __construct($f3){
        session_start();
        $conf = new Config();
        $this->db = new DB\SQL($conf->params, $conf->user,$conf->pass);
        //$f3->set('marks', $this->db->exec("SELECT * FROM `china_cars`"));
        //$f3->set('product_states', $this->db->exec("SELECT * FROM `product_states`"));
    }

    public function login(){
        $template=new Template;
        echo $template->render('ui/admin.login.html');
    }

    public function logout(){

    }

    public function home($f3){
        if (isset($_SESSION['user'])) {
            $category_images = $this->db->exec("SELECT * FROM `category_images`");
            $categories = $this->db->exec("SELECT * FROM `categories`");

            $f3->set('username', $_SESSION['user']);
            $f3->set('products', $this->db->exec("SELECT * FROM `products`"));
            $f3->set('categories', $categories);

            // detect if there are images attached to a category
            for ($i=0; $i <count($categories); $i++){
                for ($j=0; $j <count($category_images); $j++){
                    if ($categories[$i]['id'] == $category_images[$j]['category_id']){
                        $categories[$i]['image'] = $category_images[$j]['url'];
                    }
                }
            }
            ////
            $_cat_rez = $this->db->exec("SELECT * FROM `categories`");
            $cats = array();
            for ($i=0; $i <count($_cat_rez); $i++){
                $cats_ID[$_cat_rez[$i]['id']][] = $_cat_rez[$i];
                $cats[(int)$_cat_rez[$i]['parent_id']][$_cat_rez[$i]['id']] =  $_cat_rez[$i];
            }
            //
            $f3->set('ESCAPE',FALSE);
            $res = Backend::build_tree($cats,0);
            $f3->set('categoriesGrup', $res);
            //
            ////
            $f3->set('categories', $this->db->exec("SELECT * FROM `categories`"));
            $template=new Template;
            echo $template->render('ui/admin.home.html');
            $f3->set('ESCAPE',TRUE);
        }
        else { $f3->reroute("/login");  }
    }

    public function newCategory($f3){
        $f3->set('categories', $this->db->exec("SELECT * FROM `categories`"));
        $template=new Template;
        echo $template->render('ui/admin.addCategory.html');
    }

    public function editCategory($f3){
        $data=new DB\SQL\Mapper($this->db,'categories');
        $data->load(array('id=?',$f3->get('PARAMS.id')));

        $imagedata=new DB\SQL\Mapper($this->db,'category_images');
        $imagedata->load(array('category_id=?',$f3->get('PARAMS.id')));

        $f3->set('current_category',$data);
        $f3->set('current_category_image',$imagedata);
        $f3->set('categories', $this->db->exec("SELECT * FROM `categories`"));
        $template=new Template;
        echo $template->render('ui/admin.addCategory.html');
    }

    public function saveCategory($f3){
        $data=new DB\SQL\Mapper($this->db,'categories');
        $imageMapper=new DB\SQL\Mapper($this->db,'category_images');
        var_dump($_POST);
        if (isset($_POST['id'])){
            //update
            $data->load(array('id=?',$_POST['id']));
            $data->id=(int)$_POST['id'];
            $data->name=$_POST['name'];
            $data->parent_id=$_POST['parent_id'];
            $data->description=$_POST['description'];
            $data->hidden=(bool)$_POST['hidden'];
            $data->update();

            if ( isset($_FILES['image']) && ($_FILES['image']['name'] != "")) {
                $needToCreate = false;
                $image = ($this->uploadImage($f3));
                $imageMapper->load(array('category_id=?',$_POST['id']));
                if (is_null($imageMapper->category_id)) { $needToCreate = true; }

                $imageMapper->category_id = $_POST['id'];
                $imageMapper->url = $image['name'];

                if ($needToCreate) { $imageMapper->save(); } else { $imageMapper->update(); }
            }
        } else {
            // add new category
            $data->name=$_POST['name'];
            $data->parent_id=$_POST['parent_id'];
            $data->description=$_POST['description'];
            $data->hidden=(bool)$_POST['hidden'];
            $data->save();

            if ( isset($_FILES['image']) && ($_FILES['image']['name'] != "")) {
                $image = ($this->uploadImage($f3));
                $imageMapper->category_id = $this->db->lastInsertId();
                $imageMapper->url = $image['name'];
                $imageMapper->save();
            }
        }
        $f3->reroute('/admin/');
    }

    public function removeCategory($f3){
        $data=new DB\SQL\Mapper($this->db,'categories');
        /*TODO:*/
        $data->load(array('id=?',$f3->get('PARAMS.id')));
        $data->erase();
        $f3->reroute('/admin');
    }

    public function newProduct($f3){
        $product=new DB\SQL\Mapper($this->db,'products');
        $product->name = "";
        $product->category = 0;
        $product->article = 0;
        $product->price = 0;
        $product->hidden = 0;
        $product->description = "";
        $product->state = 1;
        $f3->set('product',$product);
        $f3->set('categories', $this->db->exec("SELECT * FROM `categories`"));
        $template=new Template;
        echo $template->render('ui/admin.editProduct.html');
    }

    public function editProduct($f3){
        $data=new DB\SQL\Mapper($this->db,'products');
        $data->load(array('id=?',$f3->get('PARAMS.id')));

        $imagedata=new DB\SQL\Mapper($this->db,'product_images');
        $imagedata->load(array('product_id=?',$f3->get('PARAMS.id')));

        $f3->set('product',$data);
        $f3->set('current_product_image',$imagedata);
        $f3->set('categories', $this->db->exec("SELECT * FROM `categories`"));
        $template=new Template;
        echo $template->render('ui/admin.editProduct.html');
    }

    public function writeProduct($f3){
        // new or update product
        $data=new DB\SQL\Mapper($this->db,'products');
        $imageMapper=new DB\SQL\Mapper($this->db,'product_images');
        var_dump($_POST);
        if (isset($_POST['id']) && ($_POST['id'] != "")){
            //update
            $data->load(array('id=?',$_POST['id']));
            $data->id=(int)$_POST['id'];
            $data->name=$_POST['name'];
            $data->article=$_POST['article'];
            $data->category=$_POST['category'];
            $data->price=$_POST['price'];
            $data->description=$_POST['description'];
            $data->hidden=$_POST['hidden'];
            $data->state=$_POST['state'];
            $data->update();

            if ( isset($_FILES['image']) && ($_FILES['image']['name'] != "")) {
                $needToCreate = false;
                $image = ($this->uploadImage($f3));
                $imageMapper->load(array('product_id=?',$_POST['id']));
                if (is_null($imageMapper->product_id)) { $needToCreate = true; }

                $imageMapper->product_id = $_POST['id'];
                $imageMapper->url = $image['name'];

                if ($needToCreate) { $imageMapper->save(); } else { $imageMapper->update(); }
            }
        } else {
            // add new product
            $data->name=$_POST['name'];
            $data->article=$_POST['article'];
            $data->category=$_POST['category'];
            $data->price=$_POST['price'];
            $data->description=$_POST['description'];
            $data->hidden=$_POST['hidden'];
            $data->state=$_POST['state'];
            $data->save();

            if ( isset($_FILES['image']) && ($_FILES['image']['name'] != "")) {
                $image = ($this->uploadImage($f3));
                $imageMapper->product_id = $this->db->lastInsertId();
                $imageMapper->url = $image['name'];
                $imageMapper->save();
            }
        }
        $f3->reroute('/admin/');
    }

    public function removeProduct($f3){
        print "removing product #".$f3->get('PARAMS.id');
        $data=new DB\SQL\Mapper($this->db,'products');
        $data->load(array('id=?',$f3->get('PARAMS.id')));
        $data->erase();
        $f3->reroute('/admin');
    }

    public function uploadImage($f3){
        $f3->set('UPLOADS','images/catalog/');
        $overwrite = true;
        $slug = true;
        $web = \Web::instance();
        $res = $web->receive(function($file){
                if($file['size'] > (2 * 1024 * 1024)) {return false;}
                // everything went fine, hurray!
                $file['name'] = md5(date("Y-m-d H:i:s")).".jpg";
                return true; // allows the file to be moved from php tmp dir to your defined upload dir
            },
            $overwrite,
            $slug
        );
        return $res;
    }

    public function removeCategoryImage($categoryID){
        $data=new DB\SQL\Mapper($this->db,'category_images');
        $data->load('id=?',$categoryID);
        $data->erase();
        //TODO: del the file
    }
    
    public static function build_tree($cats,$parent_id,$only_parent = false){
        if(is_array($cats) && isset($cats[$parent_id])){
            $tree = '<ul>';
            if($only_parent==false){
                foreach($cats[$parent_id] as $_cat_rez){
                    $tree .= '<li>';
                        $tree .= '<div class="categoryBlock categoryItem">';
                            $tree .= '<div class="box1p"></div><div class="box2p"><i class="fa fa-plus-circle"></i></div><div class="box1p"></div>';
                            $tree .= '<div class="box75p categoryName">'.$_cat_rez['name'].'</div>';
                            $tree .= '<div class="categoryAdmin">';
                                $tree .= '<a class="edit" href="/admin/editCategory/'.$_cat_rez['id'].'"> <i class="fa fa-pencil-square-o"></i> редактир.</a>';
                                $tree .= '<a class="del" href="/admin/removeCategory/'.$_cat_rez['id'].'"> <i class="fa fa-close"></i> удалить</a>';
                            $tree .= '</div>';
                            $tree .= '<div class="clearfix"></div>';
                        $tree .= '</div>';
                        $tree .= Backend::build_tree($cats,$_cat_rez['id']);
                    $tree .= '</li>';
                }
            }elseif(is_numeric($only_parent)){
                $_cat_rez = $cats[$parent_id][$only_parent];
                $tree .= '<li>';
                    $tree .= '<div class="categoryBlock categoryItem">';
                        $tree .= '<div class="box10p"></div><div class="box5p"></div>';
                        $tree .= '<div class="box75p categoryName">'.$_cat_rez['name'].'</div>';
                        $tree .= '<div class="categoryAdmin">';
                            $tree .= '<a class="edit" href="/admin/editCategory/'.$_cat_rez['id'].'"> <i class="fa fa-pencil-square-o"></i> редактир.</a>';
                            $tree .= '<a class="del" href="/admin/removeCategory/'.$_cat_rez['id'].'"> <i class="fa fa-close"></i> удалить</a>';
                        $tree .= '</div>';
                        $tree .= '<div class="clearfix"></div>';
                    $tree .= '</div>';
                    $tree .= build_tree($cats,$_cat_rez['id']);
                $tree .= '</li>';
            }
            $tree .= '</ul>';
        }
        else return null;
        return $tree;
    }
}