<?php
/**
 * Created by PhpStorm.
 * User: iSnaker
 * Date: 08.10.14
 * Time: 12:16
 */


class Frontend {
    private $db;
    public function __construct($f3){
        $conf = new Config();
        $this->db = new DB\SQL($conf->params, $conf->user,$conf->pass);
        //$f3->set('marks', $this->db->exec("SELECT * FROM `categories` WHERE `parent_id`=0"));
    }

    public function home($f3){
        $template=new Template;
        echo $template->render('ui/home.html');
    }

    public function china_autos($f3){
        $template=new Template;
        echo $template->render('ui/china_autos.html');
    }

    public function autoByMark($f3){
        // echo $f3->get('PARAMS.mark');

        $mark=new DB\SQL\Mapper($this->db,'china_cars');
        $mark->load(array('name=?', $f3->get('PARAMS.mark')));

        $f3->set('selected_mark', $mark);
        $template=new Template;
        echo $template->render('ui/china_autos.html');
    }

    public function reviews(){
        $template=new Template;
        echo $template->render('ui/reviews.html');
    }

    public function contacts(){
        $template=new Template;
        echo $template->render('ui/contacts.html');
    }

    /* CATALOG FUNCTIONs */

    public function catalog($f3){
        $categories = $this->db->exec("SELECT * FROM `categories` WHERE `parent_id`='0' ");
        $images = $this->db->exec("SELECT * FROM `category_images`");

        for ($i=0; $i < count($categories); $i++){
            for ($j=0; $j < count($images); $j++){
                if ($categories[$i]['id'] == $images[$j]['category_id']){
                    $categories[$i]['image'] = $images[$j]['url'];
                }
            }
        }

        $f3->set('categories', $categories);
        $template=new Template;
        echo $template->render('ui/catalog.html');
    }

    public function openCategory($f3){
        $cat = $f3->get('PARAMS.categoryId');
        $mapper=new DB\SQL\Mapper($this->db,'categories');
        $categoryImageMapper = new DB\SQL\Mapper($this->db,'category_images');

        $current = $mapper->load(array('id=?', $f3->get('PARAMS.categoryId')));
        $categoryImage = $categoryImageMapper->load(array('category_id=?', $f3->get('PARAMS.categoryId')));

        $parent = $mapper->load(array('id=?', $current->parent_id));
        $subcategories = $this->db->exec("SELECT * FROM `categories` WHERE `parent_id`='$cat'");

        for ($i=0; $i < count($subcategories); $i++){
            $image = $categoryImageMapper->load(array('category_id=?', $subcategories[$i]['id']));
            if ($image['url'] != ""){$subcategories[$i]['image'] = $image['url'];}
        }

        $products = $this->db->exec("SELECT * FROM `products` WHERE `category`='$cat'");
        $productIDS = "";
        for ($i=0; $i < count($products); $i++){
            $productIDS.=$products[$i]['id'];
            $i < count($products)-1 ? $productIDS.="," : $productIDS.="" ;
        }

        count($products) > 0 ? $option = "WHERE `product_id` IN (".$productIDS.")" : $option = "";
        $images = $this->db->exec("SELECT * FROM `product_images` $option");

        for ($i=0; $i < count($products); $i++){
            for ($j=0; $j < count($images); $j++){
                if ($products[$i]['id'] == $images[$j]['product_id']){
                    $products[$i]['image'] = $images[$j]['url'];
                }
            }
        }

        $f3->set('categories', $subcategories);
        $f3->set('current', $current);
        $f3->set('current_image', $categoryImage);
        $f3->set('parent', $parent);
        $f3->set('products', $products);

        $template=new Template;
        echo $template->render('ui/catalog.html');
    }

    public function productDetails($f3){
        $id = $f3->get('PARAMS.productId');
        $mapper=new DB\SQL\Mapper($this->db,'products');
        $categoryMapper = new DB\SQL\Mapper($this->db,'categories');
        $imageMapper = new DB\SQL\Mapper($this->db,'product_images');

        $states = $this->db->exec("SELECT * FROM `product_states`");
        $product = $mapper->load(array('id=?', $id));

        for ($i=0; $i < count($states); $i++){
            if ($product['state'] == $states[$i]['id']){
                $state = $states[$i]['name'];
                $color = $states[$i]['color'];
            }
        }

        $f3->set("product_state", $state);
        $f3->set("product_state_color", $color);
        $image = $imageMapper->load(array('product_id=?', $id));
        if ($image['url'] !== "") {$f3->set('productImage',$image['url']); }

        /* breadcrumbs */
        $cat = $categoryMapper->load( array('id=?',$product['category']));
        $parentCategory = $cat['parent_id'];

        $breadcrumbs = array();
        array_push($breadcrumbs, $cat);

        while ($parentCategory != 0){
            $cat = $categoryMapper->load( array('id=?',$parentCategory) );
            $parentCategory = $cat['parent_id'];
            array_push($breadcrumbs, $cat);
        }

        $breadcrumbs = array_reverse($breadcrumbs);
        $f3->set('breadcrumbs', ($breadcrumbs));
        $f3->set('product', $product);
        $f3->set('similar_products', $this->db->exec("SELECT * FROM `products` WHERE `category`='$product->category' AND `id`<> '$product->id' LIMIT 3"));

        $template=new Template;
        echo $template->render('ui/product.html');
    }
}
